-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2018 at 07:19 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amidstus`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `user_id` int(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `post_id` int(100) NOT NULL,
  `user_post` varchar(100) NOT NULL,
  `post_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`user_id`, `username`, `post_id`, `user_post`, `post_time`) VALUES
(1, 'Nuren Nafisa', 0, '1', '2018-02-17 20:05:53'),
(2, 'Nuren Nafisa', 0, '2', '2018-02-17 20:05:58'),
(3, 'Nuren Nafisa', 0, '3', '2018-02-17 20:06:23'),
(4, 'Nuren Nafisa', 0, '4', '2018-02-17 20:06:27'),
(5, 'Nuren Nafisa', 0, '5', '2018-02-17 20:06:30'),
(6, 'Nuren Nafisa', 0, '6', '2018-02-17 20:06:34'),
(7, 'Nuren Nafisa', 0, 'XVJ>K', '2018-02-17 20:08:05'),
(8, 'Nuren Nafisa', 0, 'dyjfl', '2018-02-17 20:08:25'),
(9, 'Nuren Nafisa', 0, 'hjkbgghkcj', '2018-02-17 20:08:44');

-- --------------------------------------------------------

--
-- Table structure for table `relationship`
--

CREATE TABLE IF NOT EXISTS `relationship` (
  `user_one_id` int(11) NOT NULL,
  `user_two_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `action_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relationship`
--

INSERT INTO `relationship` (`user_one_id`, `user_two_id`, `status`, `action_user_id`) VALUES
(1, 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sign_up`
--

CREATE TABLE IF NOT EXISTS `sign_up` (
  `user_id` int(255) NOT NULL,
  `firstname` varchar(1000) NOT NULL,
  `lastname` varchar(1000) NOT NULL,
  `username` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sign_up`
--

INSERT INTO `sign_up` (`user_id`, `firstname`, `lastname`, `username`, `email`, `password`) VALUES
(1, 'Nuren', 'Nafisa', 'Nuren Nafisa', 'nurennafisa@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(2, 'Sabrina', 'Maisha', 'Sabrina Maisha', 'sjbm1996@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_id` int(255) NOT NULL,
  `workplace` varchar(100) NOT NULL,
  `educational_institution` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `motto` varchar(100) NOT NULL,
  `profile_pic` blob NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`user_id`, `workplace`, `educational_institution`, `dob`, `motto`, `profile_pic`) VALUES
(1, 'Software Company', 'iiuc', '1995-11-02', 'Keep trust on Allah and be happy', ''),
(2, 'Software Company', 'iiuc', '1995-11-02', 'Keep trust on Allah and be happy', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`user_id`),
  ADD FULLTEXT KEY `user_post` (`user_post`);

--
-- Indexes for table `relationship`
--
ALTER TABLE `relationship`
  ADD UNIQUE KEY `user_one_id` (`user_one_id`,`user_two_id`);

--
-- Indexes for table `sign_up`
--
ALTER TABLE `sign_up`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sign_up`
--
ALTER TABLE `sign_up`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
